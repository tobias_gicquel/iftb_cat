<html lang='fr'>

<head>
	<title>Catalogue IFTB</title>
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
	<link href="https://catalogue2.iftb.fr/css/style.css" rel="stylesheet">
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="/css/norm.css" />
</head>

<body>
	<nav class="navbar navbar-expand-lg navbar-light" style='background-color: #4B0082'>
		<div class="container-fluid">
			<a class="navbar-brand" href="/" style="color: white">
				<img alt='logo iftb' src='/img/logo.png' height="50" />
			</a>
			<button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav me-auto mb-2 mb-lg-0">
					<li>
						<a style='color: white' class="btn nav-link" onclick="history.back()">Retour aux formations</a>
					</li>
				</ul>
			</div>
		</div>
	</nav>
	<div class="mt-3 container">
		<h2 style="color: #8224e3" class="display-5">Panier</h2>
	</div>

	<?php
	$act1=$_GET[action1];
	$act2=$_GET[action2];
	$act3=$_GET[action3];
	$act4=$_GET[action4];
	$act5=$_GET[action5];

	session_start();
	unset($_SESSION['panier']);
	/* Initialisation du panier */
	if(!isset($_SESSION['panier']))
	{	$_SESSION['panier'] = array();
		/* Subdivision du panier */
		$_SESSION['panier']['ida'] = array();
		$_SESSION['panier']['idf'] = array();
	}
	array_push($_SESSION['panier']['ida'],$act1, $act2, $act3, $act4, $act5);
	
	//f1
	$curl = curl_init();
		
	curl_setopt_array($curl, array(
		CURLOPT_URL => 'https://api.form-dev.fr/action/'.$act1.'',
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => '',
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 0,
		CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => 'GET',
		CURLOPT_HTTPHEADER => array(
			'Authentification: e968687c81cb3e5bc5142d7c408a77648f97312e068297f77730f31fdbaa4f0f', 'Access-Control-Allow-Origin: *'
		),
	));
		
	$responsed = curl_exec($curl);
		
	curl_close($curl);
		
	$rdad=json_decode($responsed, true);
	$resp=$rdad[Action];
	$nomf1=$resp[NomFormation];
	
	//f2
	$curl = curl_init();
		
	curl_setopt_array($curl, array(
		CURLOPT_URL => 'https://api.form-dev.fr/action/'.$act2.'',
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => '',
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 0,
		CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => 'GET',
		CURLOPT_HTTPHEADER => array(
			'Authentification: e968687c81cb3e5bc5142d7c408a77648f97312e068297f77730f31fdbaa4f0f', 'Access-Control-Allow-Origin: *'
		),
	));
		
	$responsed = curl_exec($curl);
		
	curl_close($curl);
		
	$rdad=json_decode($responsed, true);
	$resp=$rdad[Action];
	$nomf2=$resp[NomFormation];
	
	//f3
	$curl = curl_init();
		
	curl_setopt_array($curl, array(
		CURLOPT_URL => 'https://api.form-dev.fr/action/'.$act3.'',
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => '',
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 0,
		CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => 'GET',
		CURLOPT_HTTPHEADER => array(
			'Authentification: e968687c81cb3e5bc5142d7c408a77648f97312e068297f77730f31fdbaa4f0f', 'Access-Control-Allow-Origin: *'
		),
	));
		
	$responsed = curl_exec($curl);
		
	curl_close($curl);
		
	$rdad=json_decode($responsed, true);
	$resp=$rdad[Action];
	$formId=$resp[IdFormation];
	$nomf3=$resp[NomFormation];
	array_push($_SESSION['panier']['idf'],$formId);
	
	//f4
	$curl = curl_init();
		
	curl_setopt_array($curl, array(
		CURLOPT_URL => 'https://api.form-dev.fr/action/'.$act4.'',
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => '',
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 0,
		CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => 'GET',
		CURLOPT_HTTPHEADER => array(
			'Authentification: e968687c81cb3e5bc5142d7c408a77648f97312e068297f77730f31fdbaa4f0f', 'Access-Control-Allow-Origin: *'
		),
	));
		
	$responsed = curl_exec($curl);
		
	curl_close($curl);
		
	$rdad=json_decode($responsed, true);
	$resp=$rdad[Action];
	$nomf4=$resp[NomFormation];
	
	//f5
	$curl = curl_init();
		
	curl_setopt_array($curl, array(
		CURLOPT_URL => 'https://api.form-dev.fr/action/'.$act5.'',
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => '',
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 0,
		CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => 'GET',
		CURLOPT_HTTPHEADER => array(
			'Authentification: e968687c81cb3e5bc5142d7c408a77648f97312e068297f77730f31fdbaa4f0f', 'Access-Control-Allow-Origin: *'
		),
	));
		
	$responsed = curl_exec($curl);
		
	curl_close($curl);
		
	$rdad=json_decode($responsed, true);
	$resp=$rdad[Action];
	$nomf5=$resp[NomFormation];
	
	
	$curl = curl_init();

	curl_setopt_array($curl, array(
    	CURLOPT_URL => 'https://api.form-dev.fr/formation/'.$formId.'',
  	  CURLOPT_RETURNTRANSFER => true,
  	  CURLOPT_ENCODING => '',
  	  CURLOPT_MAXREDIRS => 10,
  	  CURLOPT_TIMEOUT => 0,
  	  CURLOPT_FOLLOWLOCATION => true,
  	  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  	  CURLOPT_CUSTOMREQUEST => 'GET',
  	  CURLOPT_HTTPHEADER => array(
		'Authentification: e968687c81cb3e5bc5142d7c408a77648f97312e068297f77730f31fdbaa4f0f', 'Access-Control-Allow-Origin: *'
  	  ),
	));

	$response = curl_exec($curl);

	curl_close($curl);

	$rda=json_decode($response, true);
	
	$prixf=$rda[Formation][Prix];
	
	$count=5;
	if (empty($nomf1)){
		--$count;
	};
	if (empty($nomf2)){
		--$count;
	};
	if (empty($nomf2)){
		--$count;
	};
	if (empty($nomf3)){
		--$count;
	};
	if (empty($nomf4)){
		--$count;
	};
	if (empty($nomf5)){
		--$count;
	};
	
	$gprix=$count*$prixf;
	
	?>
	<div class="container">
		<table class="table" style="border-color:white;">
			<thead class="thead-dark">
				<tr>
					<th>Module de formation</th>
					<th>Prix</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td><?php echo $nomf1 ?></td>
					<td><?php if(empty($nomf1)){} else {echo ''.$prixf.'€';}; ?></td>
				</tr>
				<tr>
					<td><?php echo $nomf2 ?></td>
					<td><?php if(empty($nomf2)){} else {echo ''.$prixf.'€';}; ?></td>
				</tr>
				<tr>
					<td><?php echo $nomf3 ?></td>
					<td><?php if(empty($nomf3)){} else {echo ''.$prixf.'€';}; ?></td>
				</tr>
				<tr>
					<td><?php echo $nomf4 ?></td>
					<td><?php if(empty($nomf4)){} else {echo ''.$prixf.'€';}; ?></td>
				</tr>
				<tr>
					<td><?php echo $nomf5 ?></td>
					<td><?php if(empty($nomf5)){} else {echo ''.$prixf.'€';}; ?></td>
				</tr>
				<tr>
					<td><mark>Total</mark></td>
					<td><?php echo $gprix ?>€</td>
				</tr>
			</tbody>
		</table>
	</div>
	<div class="container-fluid" style="background-color:#F7F7F7">
		<p style="text-align:center">Completez votre demande de devis</p>
	</div>
	<div class="container">
		<div class="rendered-form">
			<form method="post" action="/inscription.php">
				<label for="select-1634505809456" class="formbuilder-select-label">Titre<span class="formbuilder-required">*</span></label>
				<select class="form-control" name="select-1634505809456" id="select-1634505809456" required="required" aria-required="true">
					<option value="Mme" selected="true" id="select-1634505809456-0">Mme</option>
					<option value="Mr" id="select-1634505809456-1">Mr</option>
				</select>
				<label for="text-1634505864883" class="formbuilder-text-label">Nom<span class="formbuilder-required">*</span></label>
				<input type="name" class="form-control" name="text-1634505864883" access="false" id="text-1634505864883" required="required" aria-required="true">
				<label for="text-1634505865781" class="formbuilder-text-label">Prénom<span class="formbuilder-required">*</span></label>
				<input type="forname" class="form-control" name="text-1634505865781" access="false" id="text-1634505865781" required="required" aria-required="true">
				<label for="text-1634505867400" class="formbuilder-text-label">E-mail<span class="formbuilder-required">*</span></label>
				<input type="email" class="form-control" name="text-1634505867400" access="false" id="text-1634505867400" required="required" aria-required="true">
				<label for="text-16345058670" class="formbuilder-text-label">Tél.<span class="formbuilder-required">*</span></label>
				<input type="tel" class="form-control" name="text-16345058670" access="false" id="text-16345058670" required="required" aria-required="true">
				<label for="text-16345058676" class="formbuilder-text-label">Adresse<span class="formbuilder-required">*</span></label>
				<input type="adress" class="form-control" name="text-16345058676" access="false" id="text-16345058676" required="required" aria-required="true">
				<label for="text-16345058677" class="formbuilder-text-label">Code postal<span class="formbuilder-required">*</span></label>
				<input type="zipcode" class="form-control" name="text-16345058677" access="false" id="text-16345058677" required="required" aria-required="true">
				<label for="text-16345058678" class="formbuilder-text-label">Ville<span class="formbuilder-required">*</span></label>
				<input type="city" class="form-control" name="text-16345058678" access="false" id="text-16345058678" required="required" aria-required="true">
				<div class="mt-3 container-fluid text-center">
					<input style='background-color: #F7F7F7; border-color: #8224e3; color: black' type="submit" class="btn btn-primary" value="Valider votre demande de devis">
				</div>
			</form>
		</div>
	</div>

</body>

</html>