<html lang='fr'>

<head>
	<title>Catalogue IFTB</title>
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous" />
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link  href="/css/norm.css" />
</head>

<body>
	<nav class="navbar navbar-expand-lg navbar-light" style='background-color: #4B0082'>
		<div class="container-fluid">
			<a class="navbar-brand" href="/" style="color: white">
				<img alt='logo iftb' src='/img/logo.png' height="50" />
			</a>
			<button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav me-auto mb-2 mb-lg-0">
					<li class="nav-item">
						<a style='color: white' class="nav-link" href="/">Les formations</a>
					</li>
				</ul>
			</div>
		</div>
	</nav>
	<div class="mt-5 jumbotron jumbotron-fluid">
		<div class="container">
			<h1 style="color: #8224e3" class="display-6"><mark>Merci !</mark> et à bientôt</h1>
			<p style="font-size: 2.2vh" class="lead">Votre demande de devis a bien été prise en compte et nous vous recontacterons au plus vite.</p>
		</div>
	</div>
	
	<footer class="text-center text-lg-start">
		  <!-- Copyright -->
		  <div class="text-center p-3">
			Les mentions légales appliquées à ce site sont les mêmes que celles de 
			<a class="text-dark" href="https://iftb.fr/">iftb.fr</a>
		  </div>
		  <!-- Copyright -->
	</footer>
</body>

</html>