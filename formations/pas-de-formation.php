<html lang='fr'>

<head>
	<title>Catalogue IFTB</title>
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
	<link href="https://catalogue2.iftb.fr/css/style.css" rel="stylesheet">
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link  href="/css/norm.css" />
</head>

<body>
	<nav class="navbar navbar-expand-lg navbar-light" style='background-color: #4B0082'>
		<div class="container-fluid">
			<a class="navbar-brand" href="/" style="color: white">
				<img alt='logo iftb' src='/img/logo.png' height="50" />
			</a>
			<button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav me-auto mb-2 mb-lg-0">
					<li>
						<a style='color: white' class="btn nav-link" onclick="history.back()">Retour aux formations</a>
					</li>
				</ul>
			</div>
		</div>
	</nav>

	<div class="mt-5 jumbotron jumbotron-fluid">
		<div class="container">
			<h3 style="color: #8224e3"><b>Cette ville ne propose pas cette formation</b></h3>
			<p class="lead">Vous pouvez contacter François Incandela afin d'en savoir plus : 06 79 17 47 53</p>
		</div>
	</div>



</body>

</html>