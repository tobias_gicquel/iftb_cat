<html lang='fr'>

<head>
	<title>Catalogue IFTB</title>
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
	<link href="https://catalogue2.iftb.fr/css/style.css" rel="stylesheet">
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link  href="/css/norm.css" />
</head>

<body>
	<nav class="navbar navbar-expand-lg navbar-light" style='background-color: #4B0082'>
		<div class="container-fluid">
			<a class="navbar-brand" href="/" style="color: white">
				<img alt='logo iftb' src='/img/logo.png' height="50" />
			</a>
			<button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav me-auto mb-2 mb-lg-0">
					<li>
						<a style='color: white' class="btn nav-link" onclick="history.back()">Retour aux formations</a>
					</li>
				</ul>
			</div>
		</div>
	</nav>
	<div class="mt-5 jumbotron jumbotron-fluid">
		<div class="container">
			<h2 style="color: #8224e3" class="display-5"><mark>Thérapies Brèves Hypnose et PNL Méthode IFTB</mark> 5 x 2 jours</h2>
			<p class="lead"></p>
		</div>
	</div>
	<div class="mt-2 jumbotron jumbotron-fluid">
		<div class="container">
			<h4 style="color: #8224e3"><b>Modules : Les Fondamentaux, Pratiquez, Explorez</b></h4>
			<p class="lead"></p>
		</div>
	</div>

	<?php

$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => 'https://api.form-dev.fr/formation/25',
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => '',
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => 'GET',
  CURLOPT_HTTPHEADER => array(
	'Authentification: e968687c81cb3e5bc5142d7c408a77648f97312e068297f77730f31fdbaa4f0f', 'Access-Control-Allow-Origin: *'
  ),
));

$response = curl_exec($curl);

curl_close($curl);

$rda=json_decode($response, true);

?>
	<div class="container">
		<div class="row align-items-center">
			<div class="col mt-5 border-start">
				<h3>Durée</h3>
				<p>3 x 3 jours (9 jours)</p>
			</div>
			<div class="col mt-5 border-start">
				<h3>Capacité</h3>
				<p><?php echo $rda[Formation][Effectifs]; ?> personnes maximum</p>
			</div>
			<div class="col mt-5 border-start">
				<h3>Tarifs</h3>
				<p>À partir de <?php echo $rda[Formation][Prix]; ?> € Net par module</p>
			</div>
			<!-- <div class="col mt-5 border-start">
				<h3>Accessible aux P.M.R.</h3>
				<p><?php echo $rda[Formation][Accessibilite]; ?></p>
			</div> -->
		</div>
	</div>

	<div class="m-5 row justify-content-center">
		<div class="col-auto">
			<a style='background-color: #F7F7F7; border-color: #8224e3; color: black' class="btn btn-primary" href="/formations/tprog.php/?id=2">Télécharger le programme et les informations de cette formation</a>
		</div>
	</div>
	
	<?php
	
	//m1
	$curl = curl_init();
	
	curl_setopt_array($curl, array(
	  CURLOPT_URL => 'https://api.form-dev.fr/action?IdFormation=25',
	  CURLOPT_RETURNTRANSFER => true,
	  CURLOPT_ENCODING => '',
	  CURLOPT_MAXREDIRS => 10,
	  CURLOPT_TIMEOUT => 0,
	  CURLOPT_FOLLOWLOCATION => true,
	  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	  CURLOPT_CUSTOMREQUEST => 'GET',
	  CURLOPT_HTTPHEADER => array(
		'Authentification: e968687c81cb3e5bc5142d7c408a77648f97312e068297f77730f31fdbaa4f0f', 'Access-Control-Allow-Origin: *'
	  ),
	));
	
	$responsed = curl_exec($curl);
	
	curl_close($curl);
	
	$rdad=json_decode($responsed, true);
	$resp=$rdad[Actions];
	$ville=$_GET[ville];
	$ville = strtoupper($ville);
	$nbs=count($resp);
	$nbi = 0;
	while ($nbi<$nbs){
		$villem = strtoupper($resp[$nbi][Lieu]);
		if ($villem==$ville){
			$ddebut=$resp[$nbi][DateDebut];
			$jourd = substr($ddebut, -2);
			$moisd = substr($ddebut, -4, 2);
			$anneed = substr($ddebut, 0, -4);
			$dfin=$resp[$nbi][DateFin];
			$jourf = substr($dfin, -2);
			$moisf = substr($dfin, -4, 2);
			$anneef = substr($dfin, 0, -4);
			if (date("Y")==$anneed){
				if (date("m")<=$moisd){
					$date1=$resp[$nbi][Id];
					$ddate1=''.$jourd.'/'.$moisd.'/'.$anneed.' au '.$jourf.'/'.$moisf.'/'.$anneef.' à '.$rda[Formation][Prix].' €';
					$nbf++;
				};
			} else {
				if (date("Y")<$anneed) {
					$date2=$resp[$nbi][Id];
					$ddate2=''.$jourd.'/'.$moisd.'/'.$anneed.' au '.$jourf.'/'.$moisf.'/'.$anneef.' à '.$rda[Formation][Prix].' €';
					$nbf++;
				};
			};
		};
	$nbi++;
	};
	if ($nbf == 0) {
		echo '<script type="text/javascript">window.location.replace("https://catalogue.iftb.fr/formations/pas-de-formation.php");</script>';
	};
	
	//m2
	$curl = curl_init();
	
	curl_setopt_array($curl, array(
	  CURLOPT_URL => 'https://api.form-dev.fr/action?IdFormation=26',
	  CURLOPT_RETURNTRANSFER => true,
	  CURLOPT_ENCODING => '',
	  CURLOPT_MAXREDIRS => 10,
	  CURLOPT_TIMEOUT => 0,
	  CURLOPT_FOLLOWLOCATION => true,
	  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	  CURLOPT_CUSTOMREQUEST => 'GET',
	  CURLOPT_HTTPHEADER => array(
		'Authentification: e968687c81cb3e5bc5142d7c408a77648f97312e068297f77730f31fdbaa4f0f', 'Access-Control-Allow-Origin: *'
	  ),
	));
	
	$responsed = curl_exec($curl);
	
	curl_close($curl);
	
	$rdad=json_decode($responsed, true);
	$resp=$rdad[Actions];
	$ville=$_GET[ville];
	$ville = strtoupper($ville);
	$nbs=count($resp);
	$nbi = 0;
	while ($nbi<$nbs){
		$villem = strtoupper($resp[$nbi][Lieu]);
		if ($villem==$ville){
			$ddebut=$resp[$nbi][DateDebut];
			$jourd = substr($ddebut, -2);
			$moisd = substr($ddebut, -4, 2);
			$anneed = substr($ddebut, 0, -4);
			$dfin=$resp[$nbi][DateFin];
			$jourf = substr($dfin, -2);
			$moisf = substr($dfin, -4, 2);
			$anneef = substr($dfin, 0, -4);
			if (date("Y")==$anneed){
				if (date("m")<=$moisd){
					$date3=$resp[$nbi][Id];
					$ddate3=''.$jourd.'/'.$moisd.'/'.$anneed.' au '.$jourf.'/'.$moisf.'/'.$anneef.' à '.$rda[Formation][Prix].' €';
					$nbf++;
				};
			} else {
				if (date("Y")<$anneed) {
					$date4=$resp[$nbi][Id];
					$ddate4=''.$jourd.'/'.$moisd.'/'.$anneed.' au '.$jourf.'/'.$moisf.'/'.$anneef.' à '.$rda[Formation][Prix].' €';
					$nbf++;
				};
			};
		};
	$nbi++;
	};
	
	//m3
	$curl = curl_init();
	
	curl_setopt_array($curl, array(
	  CURLOPT_URL => 'https://api.form-dev.fr/action?IdFormation=27',
	  CURLOPT_RETURNTRANSFER => true,
	  CURLOPT_ENCODING => '',
	  CURLOPT_MAXREDIRS => 10,
	  CURLOPT_TIMEOUT => 0,
	  CURLOPT_FOLLOWLOCATION => true,
	  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	  CURLOPT_CUSTOMREQUEST => 'GET',
	  CURLOPT_HTTPHEADER => array(
		'Authentification: e968687c81cb3e5bc5142d7c408a77648f97312e068297f77730f31fdbaa4f0f', 'Access-Control-Allow-Origin: *'
	  ),
	));
	
	$responsed = curl_exec($curl);
	
	curl_close($curl);
	
	$rdad=json_decode($responsed, true);
	$resp=$rdad[Actions];
	$ville=$_GET[ville];
	$ville = strtoupper($ville);
	$nbs=count($resp);
	$nbi = 0;
	while ($nbi<$nbs){
		$villem = strtoupper($resp[$nbi][Lieu]);
		if ($villem==$ville){
			$ddebut=$resp[$nbi][DateDebut];
			$jourd = substr($ddebut, -2);
			$moisd = substr($ddebut, -4, 2);
			$anneed = substr($ddebut, 0, -4);
			$dfin=$resp[$nbi][DateFin];
			$jourf = substr($dfin, -2);
			$moisf = substr($dfin, -4, 2);
			$anneef = substr($dfin, 0, -4);
			if (date("Y")==$anneed){
				if (date("m")<=$moisd){
					$date5=$resp[$nbi][Id];
					$ddate5=''.$jourd.'/'.$moisd.'/'.$anneed.' au '.$jourf.'/'.$moisf.'/'.$anneef.' à '.$rda[Formation][Prix].' €';
					$nbf++;
				};
			} else {
				if (date("Y")<$anneed) {
					$date6=$resp[$nbi][Id];
					$ddate6=''.$jourd.'/'.$moisd.'/'.$anneed.' au '.$jourf.'/'.$moisf.'/'.$anneef.' à '.$rda[Formation][Prix].' €';
					$nbf++;
				};
			};
		};
	$nbi++;
	};
	
	//m4
	$curl = curl_init();
	
	curl_setopt_array($curl, array(
	  CURLOPT_URL => 'https://api.form-dev.fr/action?IdFormation=28',
	  CURLOPT_RETURNTRANSFER => true,
	  CURLOPT_ENCODING => '',
	  CURLOPT_MAXREDIRS => 10,
	  CURLOPT_TIMEOUT => 0,
	  CURLOPT_FOLLOWLOCATION => true,
	  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	  CURLOPT_CUSTOMREQUEST => 'GET',
	  CURLOPT_HTTPHEADER => array(
		'Authentification: e968687c81cb3e5bc5142d7c408a77648f97312e068297f77730f31fdbaa4f0f', 'Access-Control-Allow-Origin: *'
	  ),
	));
	
	$responsed = curl_exec($curl);
	
	curl_close($curl);
	
	$rdad=json_decode($responsed, true);
	$resp=$rdad[Actions];
	$ville=$_GET[ville];
	$ville = strtoupper($ville);
	$nbs=count($resp);
	$nbi = 0;
	while ($nbi<$nbs){
		$villem = strtoupper($resp[$nbi][Lieu]);
		if ($villem==$ville){
			$ddebut=$resp[$nbi][DateDebut];
			$jourd = substr($ddebut, -2);
			$moisd = substr($ddebut, -4, 2);
			$anneed = substr($ddebut, 0, -4);
			$dfin=$resp[$nbi][DateFin];
			$jourf = substr($dfin, -2);
			$moisf = substr($dfin, -4, 2);
			$anneef = substr($dfin, 0, -4);
			if (date("Y")==$anneed){
				if (date("m")<=$moisd){
					$date7=$resp[$nbi][Id];
					$ddate7=''.$jourd.'/'.$moisd.'/'.$anneed.' au '.$jourf.'/'.$moisf.'/'.$anneef.' à '.$rda[Formation][Prix].' €';
					$nbf++;
				};
			} else {
				if (date("Y")<$anneed) {
					$date8=$resp[$nbi][Id];
					$ddate8=''.$jourd.'/'.$moisd.'/'.$anneed.' au '.$jourf.'/'.$moisf.'/'.$anneef.' à '.$rda[Formation][Prix].' €';
					$nbf++;
				};
			};
		};
	$nbi++;
	};
	
	//m5
	$curl = curl_init();
	
	curl_setopt_array($curl, array(
	  CURLOPT_URL => 'https://api.form-dev.fr/action?IdFormation=29',
	  CURLOPT_RETURNTRANSFER => true,
	  CURLOPT_ENCODING => '',
	  CURLOPT_MAXREDIRS => 10,
	  CURLOPT_TIMEOUT => 0,
	  CURLOPT_FOLLOWLOCATION => true,
	  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	  CURLOPT_CUSTOMREQUEST => 'GET',
	  CURLOPT_HTTPHEADER => array(
		'Authentification: e968687c81cb3e5bc5142d7c408a77648f97312e068297f77730f31fdbaa4f0f', 'Access-Control-Allow-Origin: *'
	  ),
	));
	
	$responsed = curl_exec($curl);
	
	curl_close($curl);
	
	$rdad=json_decode($responsed, true);
	$resp=$rdad[Actions];
	$ville=$_GET[ville];
	$ville = strtoupper($ville);
	$nbs=count($resp);
	$nbi = 0;
	while ($nbi<$nbs){
		$villem = strtoupper($resp[$nbi][Lieu]);
		if ($villem==$ville){
			$ddebut=$resp[$nbi][DateDebut];
			$jourd = substr($ddebut, -2);
			$moisd = substr($ddebut, -4, 2);
			$anneed = substr($ddebut, 0, -4);
			$dfin=$resp[$nbi][DateFin];
			$jourf = substr($dfin, -2);
			$moisf = substr($dfin, -4, 2);
			$anneef = substr($dfin, 0, -4);
			if (date("Y")==$anneed){
				if (date("m")<=$moisd){
					$date9=$resp[$nbi][Id];
					$ddate9=''.$jourd.'/'.$moisd.'/'.$anneed.' au '.$jourf.'/'.$moisf.'/'.$anneef.' à '.$rda[Formation][Prix].' €';
					$nbf++;
				};
			} else {
				if (date("Y")<$anneed) {
					$date10=$resp[$nbi][Id];
					$ddate10=''.$jourd.'/'.$moisd.'/'.$anneed.' au '.$jourf.'/'.$moisf.'/'.$anneef.' à '.$rda[Formation][Prix].' €';
					$nbf++;
				};
			};
		};
	$nbi++;
	};

	?>
	
	
	<div class="p-3 container text-center">
		<h2 style="color: #8224e3; font-size: 32px">Dates en cours</h2>
	</div>
	
	<div class="p-3 container text-center">
		<p>Module 1 : Les fondamentaux du <?php echo $ddate1?></p>
		<p>Module 2 : Pratiquez du <?php echo $ddate3?></p>
		<p>Module 3 : Pratiquez du <?php echo $ddate5?></p>
		<p>Module 4 : Explorez du <?php echo $ddate7?></p>
		<p>Module 5 : Explorez du <?php echo $ddate9?></p>
	</div>
	
	<div class="p-3 container text-center">
		<div class="p-3 container text-center">
			<?php
			echo '<a href="/panier.php/?action1='.$date1.'&action2='.$date3.'&action3='.$date5.'&action4='.$date7.'&action5='.$date9.'" style="background-color: #F7F7F7; border-color: #F7F7F7; color: #8224e3" class="btn btn-primary">Sélectionner ces dates de formation</a>'
			?>
		</div>
	</div>
	
	<div class="p-3 container text-center">
		<h2 style="color: #8224e3; font-size: 32px">Dates à venir</h2>
	</div>
	
	<div class="p-3 container text-center">
		<p>Module 1 : Les fondamentaux du <?php echo $ddate2?></p>
		<p>Module 2 : Pratiquez du <?php echo $ddate4?></p>
		<p>Module 3 : Pratiquez du <?php echo $ddate6?></p>
		<p>Module 4 : Explorez du <?php echo $ddate8?></p>
		<p>Module 5 : Explorez du <?php echo $ddate10?></p>
	</div>
	
	<div class="p-3 container text-center">
		<?php
		echo '<a href="/panier.php/?action1='.$date2.'&action2='.$date4.'&action3='.$date6.'&action4='.$date8.'&action5='.$date10.'" style="background-color: #F7F7F7; border-color: #F7F7F7; color: #8224e3" class="btn btn-primary">Sélectionner ces dates de formation</a>'
		?>
	</div>

	<div class="p-3 container">
		<h3>Moyens pédagogiques et techniques</h3>
		<p class="mt-3"><?php echo $rda[Formation][Moyens]; ?></p>
	</div>

	<div class="p-3 container">
		<h3>Modalités d'évaluations des acquis</h3>
		<p class="mt-3"><?php echo $rda[Formation][Modalites]; ?></p>
	</div>

	<div class="p-3 container">
		<h3>Public concerné et prérequis</h3>
		<p class="mt-3"><?php echo $rda[Formation][Prerequis]; ?></p>
	</div>

	<div class="p-3 container">
		<h3>Sanctions visées</h3>
		<p class="mt-3"><?php echo $rda[Formation][SanctionsVisees]; ?></p>
	</div>

	<div class="p-3 container">
		<h3>Nature de la formation</h3>
		<p class="mt-3"><?php echo $rda[Formation][Objectif]; ?></p>
	</div>

	<div class="p-3 container">
		<h3>Qualification des intervenants</h3>
		<p class="mt-3"><?php echo $rda[Formation][QualifIntervenant]; ?></p>
	</div>

	<div class="p-3 container">
		<h3>Spécialité</h3>
		<p class="mt-3"><?php echo $rda[Formation][Specialite]; ?></p>
	</div>
	
	<div class="p-3 container">
		<h3>Délai d'accès</h3>
		<p class="mt-3"><?php echo $rda[Formation][DelaiAcces]; ?></p>
	</div>
	
	<div class="p-3 container">
		<h3>Taux satisfaction</h3>
		<p class="mt-3"><?php echo $rda[Formation][TxSatisfaction]; ?></p>
	</div>
	
	<div class="p-3 container">
		<h3>Taux validation</h3>
		<p class="mt-3"><?php echo $rda[Formation][TxValidation]; ?></p>
	</div>
	
	<div class="p-3 container">
		<h3>Accès P.M.R.</h3>
		<p><?php echo $rda[Formation][Accessibilite]; ?></p>
	</div>

</body>

</html>