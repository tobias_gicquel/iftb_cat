<?php
	$id=$_GET[id];
	
	$curl = curl_init();
	
	curl_setopt_array($curl, array(
	  CURLOPT_URL => 'https://api.form-dev.fr/formation/'.$id.'/pdf',
	  CURLOPT_RETURNTRANSFER => true,
	  CURLOPT_ENCODING => '',
	  CURLOPT_MAXREDIRS => 10,
	  CURLOPT_TIMEOUT => 0,
	  CURLOPT_FOLLOWLOCATION => true,
	  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	  CURLOPT_CUSTOMREQUEST => 'GET',
	  CURLOPT_HTTPHEADER => array(
		'Authentification: e968687c81cb3e5bc5142d7c408a77648f97312e068297f77730f31fdbaa4f0f', 'Access-Control-Allow-Origin: *'
	  ),
	));
	
	$response = curl_exec($curl);
	
	curl_close($curl);
	
	$rda=json_decode($response, true);
	$pdf=$rda[pdf];

	//Decode pdf content
	$pdfd = base64_decode($pdf, true);
	file_put_contents('Programme_Formation.pdf', $pdfd);
	
	$file = 'Programme_Formation.pdf';
	header('Content-type: application/pdf');
	header('Content-Disposition: attachment; filename="' . basename($file) . '"');
	header('Content-Transfer-Encoding: binary');
	readfile($file);
	
	
?>