<html lang='fr'>
	
	<head>
		<title>Catalogue IFTB</title>
		<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
		<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link  href="/css/norm.css" />
	</head>
	
	<body>
		<nav class="navbar navbar-expand-lg navbar-light" style='background-color: #4B0082'>
			<div class="container-fluid">
				<a class="navbar-brand" href="/" style="color: white">
					<img alt='logo iftb' src='/img/logo.png' height="50" />
				</a>
				<button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse" id="navbarSupportedContent">
					<ul class="navbar-nav me-auto mb-2 mb-lg-0">
						<li>
								<a style='color: white' class="nav-link" href="/">Retour 🏠</a>
						</li>
						<li class="nav-item">
							<a style='color: white' class="nav-link" href="/formations-iftb-Le Mans.php">Les formations à Le Mans</a>
						</li>
					</ul>
				</div>
			</div>
		</nav>
		<div class="mt-5 jumbotron jumbotron-fluid">
			<div class="container">
				<h2 style="color: #8224e3" class="display-6"><mark>Les Formations</mark> Hypnose et PNL M&eacute;thode IFTB&copy; à Le Mans</h2>
				<p class="lead"></p>
			</div>
		</div>
		<div class="container">
			<div class="row align-items-center">
				<div class="col mt-5 border-start">
					<h3 style="color: #8224e3; font-size: 22px">Formations courtes</h3>
					<h4 style="color: #431559; font-size: 16px">Thérapies Brèves Hypnose et PNL Méthode IFTB©</h4>
					<h5 class="mt-3" style="color: #431559; font-size: 16px">Formule 3 x 3 jours</h5>
					<div class="d-grid gap-2 d-md-block">
						<a href="/formations/therapies-breves-hypnose-et-pnl-methode-iftb-3x3.php?ville=Le%20Mans" style='background-color: #F7F7F7; border-color: #8224e3; color: black' class="btn btn-primary">Voir la formation</a>
					</div>
				</div>
				<div class="col mt-5 border-start">
					<h3 style="color: #8224e3; font-size: 22px">Formations intensives</h3>
					<h4 style="color: #431559; font-size: 16px">Cursus Hypnose</h4>
					<div class="mt-3">
						<a href="/formations/cursus-hypnose.php?ville=Le%20Mans" style='background-color: #F7F7F7; border-color: #8224e3; color: black' class="btn btn-primary">Voir la formation</a>
					</div>
				</div>
			</div>
		</div>
	
	</body>
	
	</html>