<html lang='fr'>

<head>
	<title>Catalogue IFTB</title>
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous" />
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link  href="/css/norm.css" />
</head>

<body>
	<nav class="navbar navbar-expand-lg navbar-light" style='background-color: #4B0082'>
		<div class="container-fluid">
			<a class="navbar-brand" href="/" style="color: white">
				<img alt='logo iftb' src='/img/logo.png' height="50" />
			</a>
			<button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav me-auto mb-2 mb-lg-0">
					<li class="nav-item">
						<a style='color: white' class="nav-link" href="/">Les formations</a>
					</li>
				</ul>
			</div>
		</div>
	</nav>
	<div class="mt-5 jumbotron jumbotron-fluid">
		<div class="container">
			<h1 style="color: #8224e3" class="display-6"><mark>Catalogue</mark> des formations IFTB©</h1>
			<p style="font-size: 2.2vh" class="lead">Notre organisme de formation, l'IFTB, met &agrave; votre disposition le catalogue de l'ensemble de ses formations &agrave; Paris, Chartres, Le Mans, Bourges, Reims, &Eacute;vreux, Arras et Aix-en-Provence. Laissez vous guider vers la bienveillance.</p>
		</div>
	</div>
	<!-- Paris, Chartres, Le Mans, Bourges, Reims, Évreux, Arras et Aix-en-Provence -->
	<div class="container align-items-center">
		<div class="row">
			<div class="col mt-5">
				<div class="card mx-auto" style="width: 16rem;">
					<div class="card-body">
						<h5 class="card-title">Paris</h5>
						<a href="/formations-iftb-paris.php" class="btn btn-primary" style='background-color: #F7F7F7; border-color: #8224e3; color: black'>Voir les formations</a>
					</div>
				</div>
			</div>
			<div class="col mt-5">
				<div class="card mx-auto" style="width: 16rem;">
					<div class="card-body">
						<h5 class="card-title">Chartres</h5>
						<a href="/formations-iftb-chartres.php" class="btn btn-primary" style='background-color: #F7F7F7; border-color: #8224e3; color: black'>Voir les formations</a>
					</div>
				</div>
			</div>
			<div class="col mt-5">
				<div class="card mx-auto" style="width: 16rem;">
					<div class="card-body">
						<h5 class="card-title">Le Mans</h5>
						<a href="/formations-iftb-le-mans.php" class="btn btn-primary" style='background-color: #F7F7F7; border-color: #8224e3; color: black'>Voir les formations</a>
					</div>
				</div>
			</div>
			<div class="col mt-5">
				<div class="card mx-auto" style="width: 16rem;">
					<div class="card-body">
						<h5 class="card-title">Bourges</h5>
						<a href="/formations-iftb-bourges.php" class="btn btn-primary" style='background-color: #F7F7F7; border-color: #8224e3; color: black'>Voir les formations</a>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col mt-5">
				<div class="card mx-auto" style="width: 16rem;">
					<div class="card-body">
						<h5 class="card-title">Reims</h5>
						<a href="/formations-iftb-reims.php" class="btn btn-primary" style='background-color: #F7F7F7; border-color: #8224e3; color: black'>Voir les formations</a>
					</div>
				</div>
			</div>
			<div class="col mt-5">
				<div class="card mx-auto" style="width: 16rem;">
					<div class="card-body">
						<h5 class="card-title">Évreux</h5>
						<a href="/formations-iftb-evreux.php" class="btn btn-primary" style='background-color: #F7F7F7; border-color: #8224e3; color: black'>Voir les formations</a>
					</div>
				</div>
			</div>
			<div class="col mt-5">
				<div class="card mx-auto" style="width: 16rem;">
					<div class="card-body">
						<h5 class="card-title">Arras</h5>
						<a href="/formations-iftb-arras.php" class="btn btn-primary" style='background-color: #F7F7F7; border-color: #8224e3; color: black'>Voir les formations</a>
					</div>
				</div>
			</div>
			<div class="col mt-5 mb-5">
				<div class="card mx-auto" style="width: 16rem;">
					<div class="card-body">
						<h5 class="card-title">Aix-en-Provence</h5>
						<a href="/formations-iftb-aix-en-provence.php" class="btn btn-primary" style='background-color: #F7F7F7; border-color: #8224e3; color: black'>Voir les formations</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<footer class="text-center text-lg-start">
		  <!-- Copyright -->
		  <div class="text-center p-3">
			Les mentions légales appliquées à ce site sont les mêmes que celles de 
			<a class="text-dark" href="https://iftb.fr/">iftb.fr</a>
		  </div>
		  <!-- Copyright -->
	</footer>
</body>

</html>